package com.example.syneproject;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SyneprojectApplication {

	public static void main(String[] args) {
		SpringApplication.run(SyneprojectApplication.class, args);
	}

}
